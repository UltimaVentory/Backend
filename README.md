# UltimaBackend

## Installation

###### Create a Virtual Environment

```shell script
python3.8 -m venv --prompt $(basename $PWD) venv
```

###### Activate the venv

```shell script
source venv/bin/activate
```

###### Install the required packages

```shell script
pip install -r requirements.txt
```
