"""
    UltimaBackend
    Copyright (C) 2020  UltimaVentory

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import os

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask_cors import CORS

__all__ = ["application"]

# Required environment variables to start the server
REQUIRED_ENV_VARS = ["FLASK_SECRET_KEY"]

BACKEND_ROOT = os.path.join(
    os.path.dirname(__file__), "../"
)  # refers to application_top

# Gets the content of the VERSION file
with open(BACKEND_ROOT + "VERSION", "r") as f:
    VERSION = f.readline().strip()

# Load environment variables from .env file
dotenv_path = os.path.join(BACKEND_ROOT, ".env")
load_dotenv(dotenv_path)

# Raises an error if a required env var isn't present
for item in REQUIRED_ENV_VARS:
    if item not in os.environ:
        raise LookupError(
            f"{item} is not set in the server's environment or .env file. It is required"
        )

# Create the base flask app
application = Flask(__name__)
application.debug = bool(os.getenv("FLASK_DEBUG", False))
application.secret_key = os.getenv("FLASK_SECRET_KEY")

# Initializes and allows the CORS
# TODO: Remove the * and replace it with real domains
CORS(application, allow_headers="*", origins="*", headers="*", expose_headers="*")


# Home route
@application.route("/")
def home_route():
    return (
        jsonify(
            {
                "version": VERSION,
                "license": """\
UltimaBackend
Copyright (C) 2020  UltimaVentory

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.\
""",
                "source": "https://gitlab.com/UltimaVentory/Backend",
                # "stats": "https://api.exploreve.fr/stats",
                # "documentation": "https://api.exploreve.fr/docs",
            }
        ),
        200,
    )
