"""
    UltimaBackend
    Copyright (C) 2020  UltimaVentory

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from ultimabackend.utils.errors import BadRequestError
from ultimabackend.utils.errors import ConflictError
from ultimabackend.utils.errors import ForbiddenError
from ultimabackend.utils.errors import NotFoundError
from ultimabackend.utils.errors import UnauthorizedError
from ultimabackend.utils.success import Success
from ultimabackend.utils.success import SuccessCreated
from ultimabackend.utils.success import SuccessDeleted
from ultimabackend.utils.success import SuccessOutput
from ultimabackend.utils.success import SuccessOutputMessage

__all__ = [
    "Success",
    "SuccessDeleted",
    "SuccessOutput",
    "SuccessOutputMessage",
    "SuccessCreated",
    "BadRequestError",
    "UnauthorizedError",
    "ForbiddenError",
    "NotFoundError",
    "ConflictError",
]
