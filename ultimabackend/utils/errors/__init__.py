"""
    UltimaBackend
    Copyright (C) 2020  UltimaVentory

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from ultimabackend.utils.errors.badrequest import BadRequestError
from ultimabackend.utils.errors.conflict import ConflictError
from ultimabackend.utils.errors.forbidden import ForbiddenError
from ultimabackend.utils.errors.notfound import NotFoundError
from ultimabackend.utils.errors.template import generate_error_json
from ultimabackend.utils.errors.unauthorized import UnauthorizedError

__all__ = [
    "BadRequestError",
    "ConflictError",
    "ForbiddenError",
    "NotFoundError",
    "UnauthorizedError",
    "generate_error_json",
]
